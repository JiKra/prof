CREATE TABLE IF NOT EXISTS customer
(
    id         INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name  VARCHAR(50) NOT NULL,
    valid_from DATETIME    NOT NULL,
    valid_to   DATETIME DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS product
(
    id         INT AUTO_INCREMENT PRIMARY KEY,
    name       VARCHAR(50) NOT NULL,
    valid_from DATETIME    NOT NULL,
    valid_to   DATETIME DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS customer_product
(
    customer_id INT      NOT NULL,
    product_id  INT      NOT NULL,
    valid_from  DATETIME NOT NULL,
    valid_to    DATETIME DEFAULT NULL,
    FOREIGN KEY (customer_id) REFERENCES customer (id),
    FOREIGN KEY (product_id) REFERENCES product (id)
);

INSERT INTO customer (id, first_name, last_name, valid_from)
VALUES (1, 'Chuck', 'Norris', now()),
       (2, 'Sylvester', 'Stallone', now()),
       (3, 'Arnold', 'Schwarzenegger', now());

INSERT INTO product (id, name, valid_from)
VALUES (1, 'iPhone', now()),
       (2, 'BMW X7', now()),
       (3, 'Orange juice', now());

INSERT INTO customer_product (customer_id, product_id, valid_from)
VALUES (1, 1, now()),
       (2, 2, now()),
       (3, 3, now());

