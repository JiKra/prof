package jikra.name.interview.prof.endpoint;


import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import jikra.name.interview.prof.facade.CustomerFacade;
import jikra.name.interview.prof.facade.api.CustomerDto;


@Endpoint
public class CustomerEndpoint {

    private static final String NAMESPACE_URI = "http://jikra.name/customers";

    private final CustomerFacade customerFacade;

    @Autowired
    public CustomerEndpoint(final CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActiveCustomersRequest")
    @ResponsePayload
    public GetActiveCustomersResponse getActiveCustomers(@RequestPayload GetActiveCustomersRequest request) {
        GetActiveCustomersResponse response = new GetActiveCustomersResponse();

        final Set<CustomerDto> customers = customerFacade.findCustomersActiveNow();

        // TODO (krakora, 23/02/2020) Generate classes, add test.

        response.setCustomers(customers);

        return response;
    }
}
