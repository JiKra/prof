package jikra.name.interview.prof;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication
public class ProfApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProfApplication.class, args);
    }

}
