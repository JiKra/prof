package jikra.name.interview.prof.service;

import static jikra.name.interview.prof.commons.Strings.fm;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import jikra.name.interview.prof.domain.Customer;
import jikra.name.interview.prof.repository.api.CustomerRepository;


@Service
public class CustomerTerminationService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerTerminationService(final CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public void terminateContract(final Long customerId, final LocalDateTime terminationDateTime) {
        final LocalDateTime now = LocalDateTime.now();
        Assert.notNull(customerId, "customerId must not be null");
        Assert.notNull(terminationDateTime, "terminationDateTime must not be null");
        Assert.isTrue(terminationDateTime.isAfter(now), "terminationDateTime is before current date time");

        final Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException(fm("Customer with id={} not found.", customerId)));

        customer.setValidTo(terminationDateTime);
        customer.getCustomerProducts()
                .forEach(customerProduct -> customerProduct.setValidTo(terminationDateTime));

        customerRepository.save(customer);
    }
}
