package jikra.name.interview.prof.service;


import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import jikra.name.interview.prof.domain.Customer;
import jikra.name.interview.prof.repository.api.CustomerRepository;


@Service
public class GetActiveCustomersService {

    private final CustomerRepository customerRepository;

    @Autowired
    public GetActiveCustomersService(final CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getActiveCustomersWithProducts() {
        return customerRepository.findAllActiveOnDateTime(LocalDateTime.now());
    }

    public List<Customer> getActiveCustomersWithProducts(final LocalDateTime dateTime) {
        Assert.notNull(dateTime, "dateTime must not be null");

        return customerRepository.findAllActiveOnDateTime(dateTime);
    }
}
