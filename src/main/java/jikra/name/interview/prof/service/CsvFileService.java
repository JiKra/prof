package jikra.name.interview.prof.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Service;

import jikra.name.interview.prof.domain.Customer;


@Service
public class CsvFileService {

    public String createAndEncryptFile(final Set<Customer> customers) throws IOException {
        final File tempFile = new File(System.getProperty("java.io.tmpdir"), getRandomFileName());

        final FileWriter out = new FileWriter(tempFile);

        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withFirstRecordAsHeader())) {
            for (Customer customer : customers) {
                try {
                    printer.printRecord(customer.getId(),
                            customer.getFirstName(),
                            customer.getLastName(),
                            customer.getCustomerProducts().size());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            printer.flush();
        }

        // TODO (krakora, 23/02/2020) Encrypt file and return location

        return tempFile.getCanonicalPath();
    }

    private static String getRandomFileName() {
        final int leftLimit = 97;
        final int rightLimit = 122;
        final int targetStringLength = 9;

        final Random random = new Random();
        final StringBuilder buffer = new StringBuilder(targetStringLength);

        for (int i = 0; i < targetStringLength; i++) {
            final int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }

        final String baseValue = buffer.toString();
        final String prefix = baseValue.substring(0, 3);
        final String suffix1 = baseValue.substring(3, 6);
        final String suffix2 = baseValue.substring(6, 9);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyMMddhhmmssSSS", Locale.getDefault());
        final String formattedDate = String.format("%s.%s", sdf.format(new Date()), random.nextInt(9));

        return "customer" + prefix + formattedDate + "." + suffix1 + "." + suffix2;
    }
}
