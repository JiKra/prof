package jikra.name.interview.prof.controller.api;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


public class CustomerTerminationJson {

    private Long customerId;

    private LocalDateTime terminationDateTime;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(final Long customerId) {
        this.customerId = customerId;
    }

    public LocalDateTime getTerminationDateTime() {
        return terminationDateTime;
    }

    public void setTerminationDateTime(final LocalDateTime terminationDateTime) {
        this.terminationDateTime = terminationDateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("customerId", customerId)
                .append("terminationDateTime", terminationDateTime)
                .toString();
    }
}
