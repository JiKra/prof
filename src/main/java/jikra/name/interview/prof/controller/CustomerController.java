package jikra.name.interview.prof.controller;


import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;
import static jikra.name.interview.prof.commons.Strings.fm;

import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import jikra.name.interview.prof.controller.api.CustomerJson;
import jikra.name.interview.prof.controller.api.CustomerProductJson;
import jikra.name.interview.prof.controller.api.CustomerTerminationJson;
import jikra.name.interview.prof.domain.Customer;
import jikra.name.interview.prof.facade.CustomerFacade;
import jikra.name.interview.prof.facade.api.CustomerDto;
import jikra.name.interview.prof.facade.api.CustomerProductDto;


/**
 * Rest controller for {@link Customer}.
 */
@RestController
public class CustomerController {

    private static final Logger logger = getLogger(CustomerController.class);

    private final CustomerFacade customerFacade;

    @Autowired
    public CustomerController(final CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    @DeleteMapping("/terminate")
    public ResponseEntity<Boolean> terminateCustomer(final @RequestBody CustomerTerminationJson terminationData) {
        logger.debug(fm("Trying to terminate customer with id={}", terminationData.getCustomerId()));
        customerFacade.terminateContract(terminationData.getCustomerId(), terminationData.getTerminationDateTime());
        return ResponseEntity.ok(true);
    }

    @GetMapping("/customer/{customerId}")
    public ResponseEntity<CustomerJson> getCustomer(final @PathVariable Long customerId) {
        return ResponseEntity.ok(getCustomerById(customerId));
    }

    @GetMapping("/customers")
    public ResponseEntity<Set<CustomerJson>> getActiveCustomers() {
        return ResponseEntity.ok(customerFacade.findCustomersActiveNow()
                .stream()
                .map(this::convert)
                .collect(Collectors.toSet()));
    }

    private CustomerJson getCustomerById(final Long customerId) {
        logger.debug(fm("Trying to find customer with id={}", customerId));
        return customerFacade.findCustomer(customerId)
                .map(this::convert)
                .orElseThrow(() -> new EntityNotFoundException(fm("Customer with ID={} not found", customerId)));
    }

    private CustomerJson convert(final CustomerDto customerDto) {
        final CustomerJson customerJson = new CustomerJson();
        customerJson.setId(customerDto.getId());
        customerJson.setFirstName(customerDto.getFirstName());
        customerJson.setLastName(customerDto.getLastName());
        customerJson.setValidFrom(customerDto.getValidFrom());
        customerJson.setValidTo(customerDto.getValidTo());
        customerJson.setProducts(customerDto.getProducts()
                .stream()
                .map(this::convert)
                .collect(toList()));
        return customerJson;
    }

    private CustomerProductJson convert(final CustomerProductDto customerProductDto) {
        final CustomerProductJson customerProductJson = new CustomerProductJson();
        customerProductJson.setProductId(customerProductDto.getProductId());
        customerProductJson.setProductName(customerProductDto.getProductName());
        customerProductJson.setValidFrom(customerProductDto.getValidFrom());
        customerProductJson.setValidTo(customerProductDto.getValidTo());
        return customerProductJson;
    }
}
