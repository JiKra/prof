package jikra.name.interview.prof.facade;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import jikra.name.interview.prof.domain.Customer;
import jikra.name.interview.prof.domain.CustomerProduct;
import jikra.name.interview.prof.facade.api.CustomerDto;
import jikra.name.interview.prof.facade.api.CustomerProductDto;
import jikra.name.interview.prof.repository.api.CustomerRepository;
import jikra.name.interview.prof.service.CustomerTerminationService;
import jikra.name.interview.prof.service.GetActiveCustomersService;


@Component
public class CustomerFacade {

    private final CustomerTerminationService customerTerminationService;

    private final GetActiveCustomersService getActiveCustomersService;

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerFacade(
            final CustomerTerminationService customerTerminationService,
            final GetActiveCustomersService getActiveCustomersService,
            final CustomerRepository customerRepository) {

        this.customerTerminationService = customerTerminationService;
        this.getActiveCustomersService = getActiveCustomersService;
        this.customerRepository = customerRepository;
    }

    @Transactional
    public void terminateContract(final Long customerId, final LocalDateTime terminationDateTime) {
        Assert.notNull(customerId, "customerId must not be null");
        Assert.notNull(terminationDateTime, "terminationDateTime must not be null");

        customerTerminationService.terminateContract(customerId, terminationDateTime);
    }

    @Transactional(readOnly = true)
    public Optional<CustomerDto> findCustomer(final Long customerId) {
        Assert.notNull(customerId, "customerId must not be null");

        return customerRepository.findById(customerId)
                .map(this::convert);
    }

    @Transactional(readOnly = true)
    public Set<CustomerDto> findCustomersActiveOnDateTime(final LocalDateTime activeOnDateTime) {
        Assert.notNull(activeOnDateTime, "activeOnDateTime must not be null");

        return getActiveCustomersService.getActiveCustomersWithProducts(activeOnDateTime)
                .stream()
                .map(this::convert)
                .collect(toSet());
    }

    @Transactional(readOnly = true)
    public Set<CustomerDto> findCustomersActiveNow() {
        return getActiveCustomersService.getActiveCustomersWithProducts()
                .stream()
                .map(this::convert)
                .collect(toSet());
    }

    private CustomerDto convert(final Customer customer) {
        final CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setValidFrom(customer.getValidFrom());
        customerDto.setValidTo(customer.getValidTo());
        customerDto.setProducts(customer.getCustomerProducts()
                .stream()
                .map(this::convert)
                .collect(toList()));
        return customerDto;
    }

    private CustomerProductDto convert(final CustomerProduct customerProduct) {
        final CustomerProductDto customerProductDto = new CustomerProductDto();
        customerProductDto.setProductId(customerProduct.getProduct().getId());
        customerProductDto.setProductName(customerProduct.getProduct().getName());
        customerProductDto.setValidFrom(customerProduct.getValidFrom());
        customerProductDto.setValidTo(customerProduct.getValidTo());
        return customerProductDto;
    }

}
