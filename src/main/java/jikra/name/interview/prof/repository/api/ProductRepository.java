package jikra.name.interview.prof.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import jikra.name.interview.prof.domain.Product;


public interface ProductRepository extends JpaRepository<Product, Long> {
}
