package jikra.name.interview.prof.repository.api;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import jikra.name.interview.prof.domain.Customer;


public interface CustomerRepository extends JpaRepository<Customer, Long> {

//    @Query(value = "select c from Customer c left join CustomerProduct cp on (cp.customer.id = c.id) and (cp.validFrom <= ?1 and (cp.validTo is null or cp.validTo >= ?1)) where (c.validFrom <= ?1 and (c.validTo is null  or c.validTo >= ?1))")
//    @Query(value = "select * from Customer c left join Customer_Product cp on (cp.customer_id = c.id) and (cp.valid_from <= :dateTime and (cp.valid_to is null or cp.valid_to >= :dateTime)) where (c.valid_from <= :dateTime and (c.valid_to is null  or c.valid_to >= :dateTime))",
//    @Query(value = "select * from Customer c left join Customer_Product cp"
//            + " on ((cp.customer_id = c.id) and (cp.valid_from <= :dateTime and cp.valid_to is null) or (cp.valid_to >= :dateTime))"
//            + " where ((c.valid_from <= :dateTime) and (c.valid_to is null or c.valid_to >= :dateTime))",
//            nativeQuery = true)
    @Query(value = "select * from Customer c left join Customer_Product cp on (cp.customer_id = c.id) and ((cp.valid_from <= :dateTime and cp.valid_to is null) or (cp.valid_to >= :dateTime)) where (c.valid_from <= :dateTime and (c.valid_to is null or c.valid_to >= :dateTime))",
            nativeQuery = true)
    List<Customer> findAllActiveOnDateTime(@Param("dateTime") LocalDateTime dateTime);
}
