package jikra.name.interview.prof.commons;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.helpers.MessageFormatter;


public final class Strings {

    private Strings() {
        throw new IllegalStateException("Strings should not be instantiated");
    }

    public static String fm(final String message, final Object... values) {
        if (StringUtils.isBlank(message) || ArrayUtils.isEmpty(values)) {
            return message;
        }

        return MessageFormatter.arrayFormat(message, values).getMessage();
    }
}
