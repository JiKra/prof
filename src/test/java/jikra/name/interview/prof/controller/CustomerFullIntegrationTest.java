package jikra.name.interview.prof.controller;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import jikra.name.interview.prof.facade.CustomerFacade;
import jikra.name.interview.prof.repository.api.CustomerRepository;
import jikra.name.interview.prof.service.CustomerTerminationService;


/**
 * Integration test for {@link CustomerController}.
 */
//@SpringBootTest(classes = {
//        CustomerController.class,
//        CustomerFacade.class,
//        CustomerTerminationService.class,
//        CustomerRepository.class
//})
//@AutoConfigureMockMvc
//@AutoConfigureTestDatabase
@Import(CustomerRepository.class)
class CustomerFullIntegrationTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Test
//    void getCustomer() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders
//                .get("/customer/1")
//                .accept(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.lastName").exists())
//                .andExpect(jsonPath("$.products").isNotEmpty());
//    }
}