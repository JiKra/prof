package jikra.name.interview.prof.repository.api;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;


/**
 * Abstract test class to provide transaction handling.
 */
@ExtendWith({SpringExtension.class})
@DataJpaTest
public abstract class AbstractJpaTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    protected <T> T runInTransaction(final TransactionCallback<T> action) {
        Assert.notNull(action, "Action may not be null.");

        return new TransactionTemplate(platformTransactionManager,
                new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW)).execute(action);
    }

}
