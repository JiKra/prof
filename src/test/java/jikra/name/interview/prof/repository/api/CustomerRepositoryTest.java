package jikra.name.interview.prof.repository.api;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Query;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jikra.name.interview.prof.domain.Customer;


/**
 * Test for {@link CustomerRepository}.
 */
class CustomerRepositoryTest extends AbstractJpaTest {

    @Autowired
    private CustomerRepository tested;

    @Test
    void given_3Customers_withValidToIsNull_expects3Customers() {
        final List<Customer> customers = tested.findAllActiveOnDateTime(LocalDateTime.now().plusHours(5));

        assertThat(customers)
                .hasSize(3);
    }

    @Test
    void given_3CustomersWith1Terminated_expects2Customers() {
        final LocalDateTime yesterday = LocalDateTime.now().minusDays(1);

        final Customer customer = getEntityManager().find(Customer.class, 1L);
        customer.setValidTo(yesterday);

        final Query query = getEntityManager().createQuery("UPDATE CustomerProduct cp SET cp.validTo = :validTo WHERE cp.customer.id = 2");
        query.setParameter("validTo", yesterday);
        query.executeUpdate();

        final List<Customer> customers = tested.findAllActiveOnDateTime(LocalDateTime.now().plusHours(5));

        assertThat(customers)
                .hasSize(2);
    }
}